<!DOCTYPE html>
<html>
<head>
<style>
.msg {
	padding: 15px;
	font-family: 'Arial';
	background: #eee;
	color: 333;
	margin-bottom: 15px;
}
.error {
	background: red;
	color: white;
}
.success {
	background: green;
	color: white;
}
</style>
</head>
<?php
require 'vendor/autoload.php';
include 'config.inc.php';

function doRequest($getField, $settings)
{
	$url = 'https://api.twitter.com/1.1/search/tweets.json';
	$requestMethod = 'GET';

	$twitter = new TwitterAPIExchange($settings);
	return $twitter->setGetfield($getField)
	->buildOauth($url, $requestMethod)
	->performRequest();
}

$done = false;
$getField = '?q=#widm%20#moltalk&count=100';
$count = 0;

while ($done === false)
{
	/**
	 * Do the request and decode it.
	 */
	$response = doRequest($getField, $settings);
	$response = json_decode($response, true);

	/**
	 * Twitter responded with an error,
	 * let's show it!
	 */
	if (isset($response['errors'][0]))
	{
		echo '<div class="msg error">';
		echo 'Er ging iets mis ('.$response['errors'][0]['code'].'): '.$response['errors'][0]['message'];
		echo '</div>';
		$done = true;
	}

	else
	{
		/**
		 * Save response to a .json file.
		 */
		$log = fopen('storage/'.$count.'.json', 'w');
		fwrite($log, json_encode($response));

		/**
		 * Show a success message.
		 */
		echo '<div class="msg success">';
		echo 'Request ' . $count . ' done!<br>';
		echo '</div>';

		/**
		 * If we didn't get a new offset, we're done.
		 */
		if (!isset($response['search_metadata']['next_results']))
		{
			$done = true;
		}

		/**
		 * We did get a new offset,
		 * do the request with the new offset and increment the count.
		 */
		else
		{
			$getField = $response['search_metadata']['next_results'];
			$count++;
			$done = false;
		}
	}
}
?>

</html>
